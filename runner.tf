# provider google in gitlab.tf
# resource google_compute_network gitlab_network in gitlab.tf
data "template_file" "runner_host" {

    template = <<EOT
        %{ if var.runner_host == "GENERATE" ~}
        "http${var.ssl_certificate != "/dev/null" ? "s" : ""}://${var.dns_name}"
        %{ else ~}
        ${var.runner_host}
        %{ endif ~}
        EOT
}

resource "google_compute_instance" "gitlab-ci-runner" {
    count = "${var.runner_count}"
    name = "${var.prefix}gitlab-ci-runner-${count.index}"
    machine_type = "${var.runner_machine_type}"
    zone = "${var.zone}"

    tags = "${var.runner_network_tags}"

    network_interface {
        // network = "${var.network}"
        subnetwork = "${var.runner_subnetwork}"
        access_config {
          // Ephemeral IP
        }
    }

    metadata = {
        sshKeys = "ubuntu:${file("${var.ssh_key}.pub")}"
    }

    connection {
        type = "ssh"
        user = "ubuntu"
        agent = "false"
        private_key = "${file("${var.ssh_key}")}"
        host = "${self.network_interface.0.access_config.0.nat_ip}"
    }

    boot_disk {
        initialize_params {
            image = "${var.image}"
            size = "${var.runner_disk_size}"
        }
    }

    provisioner "file" {
        source = "${path.module}/bootstrap_runner"
        destination = "/tmp/bootstrap_runner"
    }

    provisioner "remote-exec" {
        inline = [
            "chmod +x /tmp/bootstrap_runner",
            "sudo /tmp/bootstrap_runner ${google_compute_instance.gitlab-ci-runner[0].name} ${data.template_file.runner_host.rendered} ${data.template_file.gitlab.vars.runner_token} ${var.runner_image}"
        ]
    }

    provisioner "remote-exec" {
      when = "destroy"
      inline = [
        "sudo gitlab-ci-multi-runner unregister --name ${google_compute_instance.gitlab-ci-runner[0].name}"
      ]

    }
}

output "runner_disk_size" {
    value = "${var.runner_disk_size}"
}

output "runner_image" {
    value = "${var.runner_image}"
}

output "runner_host" {
    value = "${data.template_file.runner_host.rendered}"
}
